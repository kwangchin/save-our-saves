## v0.1

First release, as a CLI. Support for Linux with Chrome and Brave, and Windows with Edge, Chrome, and Brave.

## v0.1.1

Fixed mismatch with Chrome and Brave on Windows.
Addded Vivaldi support for windwos.
Removed unneeded dependency.
Added changelog.

## v0.1.2

Added Opera and Opera GX support for Windows.