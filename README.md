# SOS - Save Our Saves

A Node.JS Script to extract saves from browser games moved to another domain.

I should probably make this have a GUI.

## Usage

```sh
node . <os> <browser>

# possible OSes:
# linux (browsers: chrome, brave)
# windows (browsers: edge, chrome brave, vivaldi, opera, operagx)
# example: node . linux chrome
# send output to file (linux only?): node . linux chrome >> output.txt
```