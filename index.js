// Save Our Saves v0.1
// Made with <3 by Yhvr

const level = require("level");
function getSaves(path, url = "jacorb90.github.io") {
	try {
		const db = level(path);
		const found = [];
		db.createKeyStream().on("data", data => {
			if (data.startsWith(`_https://${url}`)) found.push(data);
			// I don't know if that's supposed to work or not.
			// It just does.
		}).once("end", () => {
			console.log(`${found.length} saves found!`);
			found.forEach(async save => {
				const content = await db.get(save);
				console.log(`${save.substring(9 + url.length)} - ${content}`)
			})
		});
	} catch (e) {
		console.log(`!!! ERROR !!!
SOS couldn't run.
- Did you get the path right?
- Did you close your browser first?`);
	}
}

// where localStorage is
const PATHS = {
	linux: {
		brave: "/home/{user}/.config/BraveSoftware/Brave-Browser/Default/Local Storage/leveldb",
		chrome: "/home/{user}/.config/google-chrome/Default/Local Storage/leveldb/",
	},
	windows: {
		edge: "C:/Users/{user}/AppData/Local/Microsoft/Edge/User Data/Default/Local Storage/leveldb/",
		brave: "C:/Users/{user}/AppData/Local/BraveSoftware/Brave-Browser/User Data/Default/Local Storage/leveldb/",
		opera: "C:/Users/{user}/AppData/Roaming/Opera Software/Opera GX Stable/Local Storage/leveldb/",
		chrome: "C:/Users/{user}/AppData/Local/Google/Chrome/User Data/Default/Local Storage/leveldb/",
		operagx: "C:/Users/{user}/AppData/Roaming/Opera Software/Opera Stable/Local Storage/leveldb/",
		vivaldi: "C:/Users/{user}/AppData/Local/Vivaldi/User Data/Default/Local Storage/leveldb"
	}	
};
getSaves(
	PATHS[process.argv[2]][process.argv[3]].replace(
		/\{user\}/gimu,
		require("os").userInfo().username
	),
);
